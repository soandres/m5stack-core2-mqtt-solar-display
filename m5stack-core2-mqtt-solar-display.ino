#include <M5Core2.h>
#include <PubSubClient.h>
#include <TimeLib.h>
#include <WiFi.h>
#include <I2C_BM8563.h>
#include "dialog_plain_18.h"
#include "dialog_plain_36.h"
#include "vars.h"

#define BM8563_I2C_SDA 21
#define BM8563_I2C_SCL 22

I2C_BM8563_DateTypeDef dateStruct;
I2C_BM8563_TimeTypeDef timeStruct;

WiFiUDP ntpUDP;
I2C_BM8563 rtc(I2C_BM8563_DEFAULT_ADDRESS, Wire1);
WiFiClient espClient;
PubSubClient mqtt(espClient);

int last_second;
int last_soc = 0;
int last_load = 0;
int last_soc_fill = LIGHTGREY;

void wifiSetup() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  delay(200);

  Wire1.begin(BM8563_I2C_SDA, BM8563_I2C_SCL);

  M5.Lcd.print("Wifi connecting");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    M5.Lcd.print(".");
  }
  M5.Lcd.println("");
}

void mqtt_reconnect() {

  // Loop until we're reconnected
  while (!mqtt.connected()) {
    //M5.Lcd.println("Attempting MQTT connection");
    // Attempt to connect
    if (mqtt.connect("arduinoClient")) {
      mqtt.setKeepAlive(mqtt_keepalive);
      //M5.Lcd.println("Subscribing");
      mqtt.subscribe("sofar/load_power");
      mqtt.subscribe("sofar/battery_soc");
      mqtt.subscribe("sofar/battery_power");
      mqtt.subscribe("sofar/pv_total_power");
      mqtt.subscribe("sofar/energy_storage_mode");
      mqtt.subscribe("sofar/state");
      mqtt.subscribe("sofar/grid_power");
      mqtt.subscribe("sofar/today_generation");
      mqtt.subscribe("sofar/today_import");
      mqtt.subscribe("sofar/today_export");
      mqtt.subscribe("sofar/today_consumption");
    } else {
      //M5.Lcd.print("failed, rc=");
      //M5.Lcd.print(client.state());
      //M5.Lcd.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  String data = "";
  for (int i = 0; i < length; i++) {
    //M5.Lcd.print((char)payload[i]);
    data += String((char)payload[i]);
  }

  // Column B
  int cax = 0;
  int cbx = 160;
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setFreeFont(&Dialog_plain_18);
  M5.Lcd.setTextSize(1);
  //M5.Lcd.drawFastHLine(3, 100, 255, GREEN);

  // 320x240
  if (strcmp(topic, "sofar/load_power") == 0) {
    int load = data.toInt();
    last_load = load;
    int fill = CYAN;
    if (load  >= 4500) {
      fill = RED;
    }
    else if (load >= 3000) {
      fill = ORANGE;
    }
    else if (load >= 2000) {
      fill = YELLOW;
    }
    else if (load >= 200) {
      fill = GREEN;
    }

    M5.Lcd.drawString("Load:", cax, 62, 1);
    M5.Lcd.fillRect(cax, 80, 140, 40, fill);
    M5.Lcd.setFreeFont(&Dialog_plain_36);
    M5.Lcd.setTextColor(BLACK);
    M5.Lcd.drawString(data + "w", cax + 1, 81, 1);

    M5.Lcd.fillTriangle(cax+150,110, cax+150,90, cax+140,100, fill);
    M5.Lcd.fillRect(cax+150, 95, 10, 10, fill);
    
  }
  else if (strcmp(topic, "sofar/pv_total_power") == 0) {
    int pv = data.toInt();
    int fill = LIGHTGREY;
    int tri_fill = BLACK;
    if (pv  > 0) {
      fill = GREEN;
      tri_fill = GREEN;
    }
    M5.Lcd.drawString("PV:", cbx, 0, 1);
    M5.Lcd.fillRect(cbx, 20, 160, 40, fill);
    M5.Lcd.setFreeFont(&Dialog_plain_36);
    M5.Lcd.setTextColor(BLACK);
    M5.Lcd.drawString(data + "w", cbx + 1, 21, 1);

    M5.Lcd.fillTriangle(cbx+80,70, cbx+100,70, cbx+90,79, tri_fill);
    M5.Lcd.fillRect(cbx+85, 60, 10, 10, tri_fill);
    
  }
  else if (strcmp(topic, "sofar/today_generation") == 0) {
    M5.Lcd.fillRect(58, 125, 90, 20, BLACK);
    M5.Lcd.drawString("Gen:",       cax + 1, 126, 1);
    M5.Lcd.drawString(data + "kWh", cax + 60, 126, 1);
  }
  else if (strcmp(topic, "sofar/today_import") == 0) {
    M5.Lcd.fillRect(58, 145, 90, 20, BLACK);
    M5.Lcd.drawString("Imp:",       cax + 1, 146, 1);
    M5.Lcd.drawString(data + "kWh", cax + 60, 146, 1);
  }
  else if (strcmp(topic, "sofar/today_export") == 0) {
    M5.Lcd.fillRect(58, 165, 90, 20, BLACK);
    M5.Lcd.drawString("Exp:",       cax + 1, 166, 1);
    M5.Lcd.drawString(data + "kWh", cax + 60, 166, 1);
  }
  else if (strcmp(topic, "sofar/today_consumption") == 0) {
    M5.Lcd.fillRect(58, 185, 90, 20, BLACK);
    M5.Lcd.drawString("Used:",      cax + 1, 186, 1);
    M5.Lcd.drawString(data + "kWh", cax + 60, 186, 1);
  }
  else if (strcmp(topic, "sofar/battery_soc") == 0) {
    int soc = data.toInt();
    int fill = RED;
    if (soc >= 85) {
      fill = CYAN;
    }
    else if (soc >= 50) {
      fill = GREEN;
    }
    else if (soc >= 25) {
      fill = YELLOW;
    }
    M5.Lcd.drawString("Battery:", cbx, 62, 1);
    M5.Lcd.setTextColor(BLACK);
    M5.Lcd.setFreeFont(&Dialog_plain_36);
    M5.Lcd.fillRect(cbx, 80, 90, 40, fill); 
    M5.Lcd.drawString(data + "%", cbx + 1, 81, 1);

    M5.Lcd.setFreeFont(&Dialog_plain_18);
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.fillRect(cbx, 125, 170, 20, BLACK);
    
    float avail = (battery_usable * soc) / 100;
    M5.Lcd.drawString("Avail:",              cbx + 1, 126, 1);
    M5.Lcd.drawString(String(avail) + "kWh", cbx + 70, 126, 1);

    time_t now;
    time(&now);
    float hours_remaining = (avail * 1000) / last_load;
    time_t runout = now + (hours_remaining * 3600);

    char clock_buffer[8];
    sprintf(clock_buffer, "%02d:%02d:%02d", hour(runout), minute(runout), second(runout));
    int max_discharge = (1 - depth_of_discharge) * 100;
    
    M5.Lcd.fillRect(cbx, 146, 170, 20, BLACK);
    M5.Lcd.drawString(String(max_discharge) + "%: ",cbx + 1, 146, 1);
    M5.Lcd.drawString(clock_buffer,                 cbx + 70, 146, 1);
    last_soc = soc;
    last_soc_fill = fill;
  }
  else if (strcmp(topic, "sofar/battery_power") == 0) {
    int battery_power = data.toInt();
    String symbol = "";
    if (battery_power > 0) {
      symbol = "+";
    }
   
    M5.Lcd.setTextColor(BLACK);
    M5.Lcd.fillRect(cbx + 90, 80, 90, 40, last_soc_fill);
    M5.Lcd.drawString(symbol + data + "w", cbx + 91, 91, 1);
  }
  else if (strcmp(topic, "sofar/energy_storage_mode") == 0) {
    M5.Lcd.setTextColor(BLACK);
    int fill = RED;
    if (data == "Self use") {
      fill = GREEN;
    }
    else if (data == "Time of use") {
      fill = YELLOW;
    }
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.drawString("State/Mode:",cax, 0, 1); 
    M5.Lcd.setTextColor(BLACK);
    M5.Lcd.fillRect(cax, 40, 140, 20, fill);
    M5.Lcd.drawString(data,cax + 1, 41, 1); 
  }
  else if (strcmp(topic, "sofar/state") == 0) {
    M5.Lcd.setTextColor(BLACK);
    int fill = RED;
    if (data == "On-grid") {
      fill = GREEN;
    }
    M5.Lcd.fillRect(cax, 20, 140, 20, fill); 
    M5.Lcd.drawString(data, cax + 1, 21, 1); 
  }
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setFreeFont(&Dialog_plain_18);
}

int battery_percentage() {
  const float battery_max = 4.21; //maximum voltage of battery
  const float battery_min = 3.5;  //battery fades after this point.
  // https://community.m5stack.com/topic/2994/core2-how-to-know-the-current-battery-level/6
  
  int percent = ((M5.Axp.GetBatVoltage() - battery_min) / (battery_max - battery_min)) * 100;
  if (percent < 0) { 
    percent = 0; 
  }
  return percent;
}

void setup() {
  M5.begin(true, false, true); // LCDEnable, SDEnable, SerialEnable, I2CEnable, mode
  M5.Lcd.fillScreen(RED);
  M5.Axp.SetLcdVoltage(2700); // 2500-3300

  M5.Lcd.setTextSize(1);
  M5.Lcd.println("Starting");

  //Serial.begin(115200);
  M5.Lcd.setSwapBytes(true);

  M5.Lcd.println("Wifi setup");
  wifiSetup();
  rtc.begin();

  M5.Lcd.println("NTP setup");
// Set ntp time to local
  configTime(utc_offset, 0, ntp_server);

  // Init I2C
  Wire1.begin(BM8563_I2C_SDA, BM8563_I2C_SCL);

  // Init RTC
  rtc.begin();

  // Get local time
  struct tm timeInfo;
  if (getLocalTime(&timeInfo)) {
    // Set RTC time
    timeStruct.hours   = timeInfo.tm_hour;
    timeStruct.minutes = timeInfo.tm_min;
    timeStruct.seconds = timeInfo.tm_sec;
    rtc.setTime(&timeStruct);

    // Set RTC Date
    dateStruct.weekDay = timeInfo.tm_wday;
    dateStruct.month   = timeInfo.tm_mon + 1;
    dateStruct.date    = timeInfo.tm_mday;
    dateStruct.year    = timeInfo.tm_year + 1900;
    rtc.setDate(&dateStruct);
  }

  M5.Lcd.println("MQTT setup");
  IPAddress server;

  server.fromString(mqtt_broker);
  mqtt.setServer(server, 1883);
  mqtt.setCallback(mqtt_callback);

  if (!mqtt.connected()) {
    mqtt_reconnect();
  }

  M5.Lcd.fillScreen(BLACK);
  //M5.Lcd.fillScreen(M5.Lcd.alphaBlend(128, WHITE, BLACK));
  M5.Lcd.setFreeFont(&Dialog_plain_18);
  //M5.Lcd.setFreeFont(&Dialog_plain_36);
}

void loop() {
  rtc.getDate(&dateStruct);
  rtc.getTime(&timeStruct);

  if (WiFi.status() != WL_CONNECTED) {
    wifiSetup();
  }
  if (!mqtt.connected()) {
    mqtt_reconnect();
  }

  // Update once a second
  if (timeStruct.seconds != last_second) {
    M5.Lcd.fillRect(0, 215, 320, 30, BLACK);
    M5.Lcd.setFreeFont(&Dialog_plain_18);
    M5.Lcd.setTextColor(WHITE);

    float batVoltage = M5.Axp.GetBatVoltage();

    char clock_buffer[8];
    sprintf(clock_buffer, "%02d:%02d:%02d", timeStruct.hours, timeStruct.minutes, timeStruct.seconds);
    M5.Lcd.drawString(clock_buffer, 115, 222, 1);

    int fill = WHITE;
    if (M5.Axp.isCharging()) {
      fill = GREEN;
    }
    M5.Lcd.setTextColor(fill);
    //M5.Lcd.fillRect(0, 215, 65, 30, BLACK);
    M5.Lcd.drawString(String(battery_percentage()) + "%", 1, 222, 1);

    int wifi_fill = RED;
    if (WiFi.status()) {
      wifi_fill = GREEN;
    }
    int mq_fill = RED;
    if (mqtt.connected()) {
      mq_fill = GREEN;
    }
    //M5.Lcd.fillRect(234, 215, 150, 30, BLACK);
    M5.Lcd.setTextColor(wifi_fill);
    M5.Lcd.drawString("WIFI", 238, 222, 1);
    M5.Lcd.setTextColor(mq_fill);
    M5.Lcd.drawString("MQ", 290, 222, 1);
    M5.Lcd.setTextColor(WHITE);
  }
  last_second = timeStruct.seconds;
  delay(10); // Saves battery
  mqtt.loop();
  M5.update();
}
