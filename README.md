# M5Stack Core2 MQTT Solar Display

## Description

A small project to display solar information from MQTT.

![Solar Display](solar_display.png)

## Getting started

1. Follow the instructions [here](https://docs.m5stack.com/en/quick_start/core2/arduino) to get Arduino IDE setup

1. Install these additional libraries:
* [M5Core2](https://github.com/m5stack/M5Core2) - 0.0.9
* [PubSubClient](https://pubsubclient.knolleary.net/) - 2.8.0 
* [Wifi](https://www.arduino.cc/reference/en/libraries/wifi/) - 1.2.7
* [I2C_BM8563](https://github.com/tanakamasayuki/I2C_BM8563) - 1.0.3
* [TimeLib](https://playground.arduino.cc/Code/Time/) - 1.6.1

Note: I tried Arduino IDE 2.x and newer version of M5Core but they just didn't work for me.

3. Update the vars.h file with your settings

## Roadmap

Not ordered.

1. Projected run out time based on SOC
1. Indicator for sufficient energy to run high loads (washing machine, tumbler dryer) to reach next Octopus Go period or new Solar start time
1. Next day solar forecast
1. Inverter Time of use settings 
1. Battery cycle protection (charge current limit based on voltage)
1. Multiple screens to show different data
1. Hot water on/off indicator
1. Hot water on/off button
1. Live/Stale data indicator
1. Current temperature
1. Alarm when energy usage is over inverter capacity (grid draw)
1. Auto brightness
1. Improve flexibility/re-use to allow data to be shown in different areas


## Contributing

This is primarily for my own enjoyment but if you want to contribute please feel welcome. 

## Authors and acknowledgment

This project was inspired and borrowed code from [Volos' Chuck Norris clock](https://github.com/VolosR/ChuchNorrisClockM5Stack/)

## Project status

ALPHA - Experimental and under continual development
